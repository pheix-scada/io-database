#ifndef __IODB_TEST_H
#define __IODB_TEST_H

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <io_db.h>
#include <io_db_ver.h>

#include "iodb-test-tags.h"

#define CONTROLLER   72563

double normalize(double x, unsigned int digits);

extern void io_db_pg_version(void);
extern int io_db_pg_srv_version(void);
extern struct mlcp_tags * io_db_init_tags(unsigned int cntrl_id, char *tagnames[], int len);
extern int io_db_set_tags(struct mlcp_tags * mlcp_t);
extern void io_db_free_tags(struct mlcp_tags * mlcp_t);
extern int io_db_import_tags(struct mlcp_tags * mlcp_t, double * values, int len);

#endif // __IODB_TEST_H
