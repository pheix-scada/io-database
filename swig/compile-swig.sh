#!/bin/sh

PERLINCLUDES=`perl -e 'use Config; print "$Config{archlib}\n";'`
gcc -fpic -c -Dbool=char -I../debug/lib/include/ `pkg-config --cflags libpq` -I${PERLINCLUDES}/CORE iodb_wrap.c iodb.c -D_GNU_SOURCE
gcc -shared iodb.o iodb_wrap.o `pkg-config --libs libpq` -L../debug/lib/ -liodb-x64 -lpthread -lpq -o IODB.so
