# IO-database: IO server universal database access library

This is minimalistic client library for IO server universal database access.

## Build IO-database

**IO-database** is dependable on [pthread](https://www.cs.cmu.edu/afs/cs/academic/class/15492-f07/www/pthreads.html) and [pq](https://postgrespro.ru/docs/postgresql/9.6/libpq) libraries, so you need them pre-installed.

Build command:

```bash
$ make
```

## License

**IO-database** is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
