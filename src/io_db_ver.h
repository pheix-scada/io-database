#ifndef __IO_DB_VER_H
#define __IO_DB_VER_

#define IODB_VERSION_MAJOR   0
#define IODB_VERSION_MINOR   0
#define IODB_VERSION_RELEASE 46
#define IODB_VERSION_DEV     0

#endif // __IO_DB_VER_
