#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

use Data::Dumper;

use lib './';
use IODB;

#my $retcode = IODB::iodb_rnd_insert();

my $ctrlid    = 72563;
my $rndctrlid = 574;

my %params;

my @params_keys = (
    "Qb_AD2",
    "Qb_BKHC",
    "Qb_DHC",
    "Qb_DKC",
    "Qb_KP",
    "Qb_TP20",
    "Qc_AD",
    "Qc_AD2",
    "Qc_BKHC",
    "Qc_DHC",
    "Qc_DKC",
    "Qc_TP20",
    "Sa_AD",
    "Sa_AD2",
    "Sa_BKHC",
    "Sb_AD",
    "Sb_AD2",
    "Sb_BKHC",
    "Sb_DHC",
    "Sb_DKC",
    "Sb_KP",
    "Sb_TP20",
    "Sc_AD",
    "Sc_AD2",
    "Sc_BKHC",
    "Sc_DHC",
    "Sc_DKC",
    "Sc_KP",
    "Sc_TP20",
    "Ua_AD",
    "Ua_AD2",
    "Ua_BKHC",
    "Ua_DKC",
    "Ua_KP",
    "Ua_TP20",
    "Ub_AD",
    "Ub_AD2",
    "Ub_BKHC",
    "Ub_DKC",
    "Ub_KP",
    "Ub_TP20",
    "Uc_AD",
    "Uc_AD2",
    "Uc_BKHC",
    "Uc_DKC",
    "Uc_KP",
    "cosFi_AD",
    "cosFi_BKHC",
    "cosFi_DHC",
    "cosFi_DKC",
    "cosFi_KP",
    "cosFi_TP20",
    "tag_close_AD",
    "tag_close_AD2",
    "tag_close_BKHC",
    "tag_close_DHC",
    "tag_close_DKC",
    "tag_close_KP",
    "tag_close_TP20",
    "tag_close_bus",
    "WIRE_on"
);

for my $key (@params_keys) {
    $params{$key} = sprintf("%.02f", rand(100));
}

my @keys = keys %params;
my @vals = values %params;

my $rndval_ref = IODB::iodb_debug_out(\@keys, $#keys + 1);
if (defined $rndval_ref && @$rndval_ref) {
    print join(q{;}, @$rndval_ref) . "\n";
}
else {
    die "IODB: debug out double array failure!\n";
}

my $retcode = IODB::iodb_insert(\@keys, \@vals, $#keys + 1, $ctrlid);

if (!$retcode) {
    die "IODB: generic insert to Postgres failure!\n";
}
else {
    my $ref_1 = IODB::iodb_select(\@keys, $#keys + 1, $ctrlid);

    my $tstok = 1;
    if (defined $ref_1 && @$ref_1) {
        print join(q{;}, @$ref_1) . "\n";
        for my $i (0..$#vals) {
            if ($vals[$i] != @$ref_1[$i]) {
                $tstok = 0;
            }
        }
        if (!$tstok) {
            die "IODB: insert->select validation failure!\n";
        }
        else {
            print "IODB: generic tags are successfully inserted to Postgres\n";
            IODB::iodb_debug_in(\@keys, \@vals, $#keys + 1);
            $retcode = IODB::iodb_rnd_insert($rndctrlid);
            if (!$retcode) {
                die "IODB: random insert to Postgres failure!\n";
            }
            else {
                print "IODB: random tags are successfully inserted to Postgres\n";
            }
        }
    }
    else {
        die "IODB: tags select failure!\n";
    }
}
