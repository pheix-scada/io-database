#include <io_db_structs.h>

struct mlcp_tags *
io_db_init_tags(unsigned int cntrl_id, char *tagnames[], int len)
{
    int i, j;
    struct mlcp_tags * mlcp_t = NULL;
    struct tag       * tags   = NULL;
    mlcp_t = calloc( 1, sizeof(mlcp_tags) );
    if ( sem_init ( &mlcp_t->data_sync_sem, 0, 1 ) != 0) {
        fprintf(
            stderr,
            "***WRN: sem_init failed %d {%s}\n",
            errno,
            strerror(errno)
        );
    }
    if ( len > 0 ) {
        tags   = calloc( len, sizeof(tag) );
        if ( mlcp_t != NULL && tags != NULL) {
            mlcp_t->len = len;
            mlcp_t->controller_id = cntrl_id;
            for( i=0; i<len; i++ ) {
                char * lowercase_name = strdup(tagnames[i]);

                for (j = 0; j < strlen(tagnames[i]); j++) {
                    lowercase_name[j] = tolower(tagnames[i][j]);
                }

                strcpy(tags[i].tagname, lowercase_name);
            }
            mlcp_t->tags = tags;
        }
    }
    return mlcp_t;
}

void
io_db_free_tags(struct mlcp_tags * mlcp_t)
{
    if ( mlcp_t != NULL ) {
        if ( mlcp_t->tags != NULL ) {
            free( mlcp_t->tags );
        }
        if ( sem_destroy(&mlcp_t->data_sync_sem) != 0 ) {
            fprintf(
                stderr,
                "***WRN: sem_destroy failed %d {%s}\n",
                errno,
                strerror(errno)
            );
        }
        free(mlcp_t);
    }
}

void
io_db_print_tags(struct mlcp_tags * mlcp_t)
{
    int i;
    if ( mlcp_t != NULL ) {
        for( i=0; i < mlcp_t->len; i++ ) {
            printf(
                "\t%03d: %-16s => %024.05f\n",
                i,
                mlcp_t->tags[i].tagname,
                (double)mlcp_t->tags[i].val
            );
        }
    }
}

double
io_db_access_tag(
    struct mlcp_tags * mlcp_t,
    char *tagname,
    unsigned int access_mode,
    double value
) {
    double rc = -1;
    int    i;
    if ( mlcp_t != NULL ) {
        if ( sem_trywait(&mlcp_t->data_sync_sem) != 0 ) {
            fprintf(
                stderr,
                "***WRN: sem_trywait failed %d {%s}\n",
                errno,
                strerror(errno)
            );
        }
        else {
            if ( tagname != NULL ) {
                if ( strlen(tagname) < TAG_LEN ) {
                    for ( i = 0; i < mlcp_t->len; i++ ) {
                        if ( mlcp_t->tags != NULL ) {
                            if (
                                strcmp(
                                    mlcp_t->tags[i].tagname,
                                    tagname
                                ) == 0
                            ) {
                                if ( access_mode == WRITEMODE ) {
                                    mlcp_t->tags[i].val = value;
                                }
                                else {
                                    rc = mlcp_t->tags[i].val;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            if (sem_post( &mlcp_t->data_sync_sem) != 0 ) {
                fprintf(
                    stderr,
                    "***WRN: sem_post failed %d {%s}\n",
                    errno,
                    strerror(errno)
                );
            }
        }
    }
    return rc;
}

int
io_db_import_tags(
    struct mlcp_tags * mlcp_t,
    double * values,
    int len
) {
    int i;
    int rc = 1;
    if (mlcp_t != NULL) {
        if (mlcp_t->len == len) {
            for (i = 0; i < mlcp_t->len; i++) {
                if (mlcp_t->tags != NULL && values != NULL) {
                    mlcp_t->tags[i].val = values[i];
                }
            }
        }
        else {
            rc = 0;
        }
    }
    else {
        rc = 0;
    }
    return rc;
}
