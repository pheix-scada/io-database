#include "iodb-test.h"

int
main(int argc, char *argv[])
{
    int i = 0;
    char devstr[TEST_STR_SZ];

    srand(time(NULL));
    double values[TEST_SENSORS];

    for (i = 0; i < TEST_SENSORS; i++) {
        values[i] = normalize((double)rand()/RAND_MAX, 5);
        //printf("%s=%f\n", names[i], values[i]);
    };

    if (IODB_VERSION_DEV) {
        sprintf(devstr, ".dev-%d", IODB_VERSION_DEV);
    }

    printf(
        "***INF (0x%08x): libiodb version: %d.%d.%d%s\n",
        (int)pthread_self(),
        IODB_VERSION_MAJOR,
        IODB_VERSION_MINOR,
        IODB_VERSION_RELEASE,
        ( IODB_VERSION_DEV > 0 ) ? devstr : ""
    );

    io_db_pg_version();

    if (!io_db_pg_srv_version()) {
        fprintf(
            stderr,
            "***ERR (0x%08x): Postgres is unavailable\n",
            (int)pthread_self()
        );
    };

    struct mlcp_tags * mlcp_t =
        io_db_init_tags( CONTROLLER, names, TEST_SENSORS );

    if (mlcp_t != NULL) {
        io_db_import_tags(mlcp_t, (double *)values, TEST_SENSORS);

        if (!io_db_set_tags(mlcp_t)) {
            fprintf(
                stderr,
                "***ERR (0x%08x): Potrgres set request failure\n",
                (int)pthread_self()
            );
        }
        else {
            if (!io_db_get_tags(mlcp_t)) {
                fprintf(
                    stderr,
                    "***ERR (0x%08x): Potrgres get request failure\n",
                    (int)pthread_self()
                );
            }
            else {
                char * magic_names[1] = { TEST_MAGIC_TAG_NAME };

                struct mlcp_tags * mlcp_update =
                    io_db_init_tags(CONTROLLER, magic_names, 1);

                if (mlcp_update != NULL) {
                    mlcp_update->tags[0].val = TEST_MAGIC_TAG_DATA;

                    if(io_db_set_tags(mlcp_update) && io_db_get_tags(mlcp_t)) {
                        int check = 1;

                        for (i = 0; i < TEST_SENSORS; i++) {
                            if (strcmp(mlcp_t->tags[i].tagname, TEST_MAGIC_TAG_NAME) == 0) {
                                if (mlcp_t->tags[i].val != TEST_MAGIC_TAG_DATA) {
                                    check = 0;

                                    fprintf(
                                        stderr,
                                        "***ERR (0x%08x): magic tag %s != %f, %f\n",
                                        (int)pthread_self(),
                                        mlcp_t->tags[i].tagname,
                                        mlcp_t->tags[i].val,
                                        TEST_MAGIC_TAG_DATA
                                    );

                                    break;
                                }
                            }
                            else {
                                if (mlcp_t->tags[i].val != values[i]) {
                                    check = 0;

                                    fprintf(
                                        stderr,
                                        "***ERR (0x%08x): tag %s != %f, %f\n",
                                        (int)pthread_self(),
                                        mlcp_t->tags[i].tagname,
                                        values[i],
                                        mlcp_t->tags[i].val
                                    );

                                    break;
                                }
                            }
                        }

                        if (check) {
                            printf("***INF (0x%08x): sequental update test is passed\n", (int)pthread_self());

                            io_db_free_tags(mlcp_update);

                            return 0;
                        }
                    }

                    io_db_free_tags(mlcp_update);
                }
            }
        }

        io_db_free_tags(mlcp_t);
    }

    return 1;
}

double normalize(double x, unsigned int digits) {
    double fac = pow(10, digits);

    return round(x*fac)/fac;
}
