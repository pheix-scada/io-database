%module IODB

%typemap(in) char ** {
	AV *tempav;
	I32 len;
	int i;
	SV  **tv;
	if (!SvROK($input))
	    croak("Argument $argnum is not a reference.");
        if (SvTYPE(SvRV($input)) != SVt_PVAV)
	    croak("Argument $argnum is not an array.");
        tempav = (AV*)SvRV($input);
	len = av_len(tempav);
	$1 = (char **) malloc((len+2)*sizeof(char *));
	for (i = 0; i <= len; i++) {
	    tv = av_fetch(tempav, i, 0);
	    $1[i] = (char *) SvPV(*tv,PL_na);
        }
	$1[i] = NULL;
};

%typemap(freearg) char ** {
	free($1);
}

%typemap(in) double * {
	AV *tempav;
	I32 len;
	int i;
	SV  **tv;
	if (!SvROK($input))
	    croak("Argument $argnum is not a reference.");
        if (SvTYPE(SvRV($input)) != SVt_PVAV)
	    croak("Argument $argnum is not an array.");
        tempav = (AV*)SvRV($input);
	len = av_len(tempav);
	$1 = (double *) malloc((len+2)*sizeof(double));
	for (i = 0; i <= len; i++) {
        tv = av_fetch(tempav, i, 0);
        $1[i] = (double) SvNV(*tv);
        }
	$1[i] = 0;
};

%typemap(freearg) double * {
	free($1);
}

%typemap(out) double * {
    AV *myav;
    SV **svs;
    int len = arg2;
	svs = (SV **) malloc(len*sizeof(SV *));
    int i;
    for (i = 0; i < len; i++) {
        svs[i] = newSVnv($1[i]);
    }
	myav = av_make(len, svs);
    free(svs);
    $result = newRV_noinc((SV*)myav);
    sv_2mortal($result);
    argvi++;
}

%{
extern int set_controller_id(int cntrl_id);
extern int get_controller_id(void);
extern int iodb_insert(char **keys, double * values, int len, int cntrl_id);
extern int iodb_debug_in(char **keys, double * values, int len);
extern int iodb_rnd_insert(int cntrl_id);
extern double * iodb_debug_out(char **keys, int len);
extern double * iodb_select(char **keys, int len, int cntrl_id);
%}
extern int set_controller_id(int cntrl_id);
extern int get_controller_id(void);
extern int iodb_insert(char **keys, double * values, int len, int cntrl_id);
extern int iodb_debug_in(char **keys, double * values, int len);
extern int iodb_rnd_insert(int cntrl_id);
extern double * iodb_debug_out(char **keys, int len);
extern double * iodb_select(char **keys, int len, int cntrl_id);
