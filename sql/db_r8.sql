DROP TABLE if exists s0052381_tags;
DROP TABLE if exists s0052382_tags;
DROP TABLE if exists s0052383_tags;
DROP TABLE if exists s0052384_tags;
DROP TABLE if exists s0052385_tags;
DROP TABLE if exists s0052386_tags;

CREATE TABLE s0052381_tags (
    id               SERIAL PRIMARY KEY,
    timestamp        char(20) DEFAULT extract(epoch from now()),
    baget_plk_DI_1_1 NUMERIC,
    baget_plk_DI_1_2 NUMERIC,
    baget_plk_DI_1_3 NUMERIC,
    baget_plk_DI_1_4 NUMERIC,
    baget_plk_DI_1_5 NUMERIC,
    baget_plk_DI_1_6 NUMERIC,
    baget_plk_DI_1_7 NUMERIC,
    baget_plk_DI_1_8 NUMERIC,
    baget_plk_DI_2_1 NUMERIC,
    baget_plk_DI_2_2 NUMERIC,
    baget_plk_DI_2_3 NUMERIC,
    baget_plk_DI_2_4 NUMERIC,
    baget_plk_DI_2_5 NUMERIC,
    baget_plk_DI_2_6 NUMERIC,
    baget_plk_DI_2_7 NUMERIC,
    baget_plk_DI_2_8 NUMERIC
);

CREATE TABLE s0052382_tags (
    id               SERIAL PRIMARY KEY,
    timestamp        char(20) DEFAULT extract(epoch from now()),
    baget_plk_DO_1_1 NUMERIC,
    baget_plk_DO_1_2 NUMERIC,
    baget_plk_DO_1_3 NUMERIC,
    baget_plk_DO_1_4 NUMERIC,
    baget_plk_DO_1_5 NUMERIC,
    baget_plk_DO_1_6 NUMERIC,
    baget_plk_DO_1_7 NUMERIC,
    baget_plk_DO_1_8 NUMERIC,
    baget_plk_DO_2_1 NUMERIC,
    baget_plk_DO_2_2 NUMERIC,
    baget_plk_DO_2_3 NUMERIC,
    baget_plk_DO_2_4 NUMERIC,
    baget_plk_DO_2_5 NUMERIC,
    baget_plk_DO_2_6 NUMERIC,
    baget_plk_DO_2_7 NUMERIC,
    baget_plk_DO_2_8 NUMERIC
);

CREATE TABLE s0052383_tags (
    id               SERIAL PRIMARY KEY,
    timestamp        char(20) DEFAULT extract(epoch from now()),
    baget_uso_DI_1_1 NUMERIC,
    baget_uso_DI_1_2 NUMERIC,
    baget_uso_DI_1_3 NUMERIC,
    baget_uso_DI_1_4 NUMERIC,
    baget_uso_DI_1_5 NUMERIC,
    baget_uso_DI_1_6 NUMERIC,
    baget_uso_DI_1_7 NUMERIC,
    baget_uso_DI_1_8 NUMERIC,
    baget_uso_DI_2_1 NUMERIC,
    baget_uso_DI_2_2 NUMERIC,
    baget_uso_DI_2_3 NUMERIC,
    baget_uso_DI_2_4 NUMERIC,
    baget_uso_DI_2_5 NUMERIC,
    baget_uso_DI_2_6 NUMERIC,
    baget_uso_DI_2_7 NUMERIC,
    baget_uso_DI_2_8 NUMERIC,
    baget_uso_DI_3_1 NUMERIC,
    baget_uso_DI_3_2 NUMERIC,
    baget_uso_DI_3_3 NUMERIC,
    baget_uso_DI_3_4 NUMERIC,
    baget_uso_DI_3_5 NUMERIC,
    baget_uso_DI_3_6 NUMERIC,
    baget_uso_DI_3_7 NUMERIC,
    baget_uso_DI_3_8 NUMERIC,
    baget_uso_DI_4_1 NUMERIC,
    baget_uso_DI_4_2 NUMERIC,
    baget_uso_DI_4_3 NUMERIC,
    baget_uso_DI_4_4 NUMERIC,
    baget_uso_DI_4_5 NUMERIC,
    baget_uso_DI_4_6 NUMERIC,
    baget_uso_DI_4_7 NUMERIC,
    baget_uso_DI_4_8 NUMERIC,
    plottag          NUMERIC,
    voltage1         NUMERIC,
    voltage2         NUMERIC,
    hour             NUMERIC,
    minute           NUMERIC,
    second           NUMERIC,
    day              NUMERIC,
    month            NUMERIC,
    year             NUMERIC,
    temperature      NUMERIC,
    plc              NUMERIC,
    btn_on           NUMERIC,
    btn_off          NUMERIC,
    led_on           NUMERIC,
    led_off          NUMERIC
);

CREATE TABLE s0052384_tags (
    id               SERIAL PRIMARY KEY,
    timestamp        char(20) DEFAULT extract(epoch from now()),
    baget_uso_DO_1_1 NUMERIC,
    baget_uso_DO_1_2 NUMERIC,
    baget_uso_DO_1_3 NUMERIC,
    baget_uso_DO_1_4 NUMERIC,
    baget_uso_DO_1_5 NUMERIC,
    baget_uso_DO_1_6 NUMERIC,
    baget_uso_DO_1_7 NUMERIC,
    baget_uso_DO_1_8 NUMERIC
);

CREATE TABLE s0052385_tags (
    id                SERIAL PRIMARY KEY,
    timestamp         char(20) DEFAULT extract(epoch from now()),
    baget_uso1_AO_1_1 NUMERIC,
    baget_uso1_AO_1_2 NUMERIC,
    baget_uso1_AO_1_3 NUMERIC,
    baget_uso1_AO_1_4 NUMERIC,
    baget_uso1_AO_1_5 NUMERIC,
    baget_uso1_AO_1_6 NUMERIC,
    baget_uso1_AO_1_7 NUMERIC,
    baget_uso1_AO_1_8 NUMERIC
);

CREATE TABLE s0052386_tags (
    id                SERIAL PRIMARY KEY,
    timestamp         char(20) DEFAULT extract(epoch from now()),
    baget_uso1_AI_1_1 NUMERIC,
    baget_uso1_AI_1_2 NUMERIC,
    baget_uso1_AI_1_3 NUMERIC,
    baget_uso1_AI_1_4 NUMERIC,
    baget_uso1_AI_1_5 NUMERIC,
    baget_uso1_AI_1_6 NUMERIC,
    baget_uso1_AI_1_7 NUMERIC,
    baget_uso1_AI_1_8 NUMERIC,
    baget_uso1_AI_2_1 NUMERIC,
    baget_uso1_AI_2_2 NUMERIC,
    baget_uso1_AI_2_3 NUMERIC,
    baget_uso1_AI_2_4 NUMERIC,
    baget_uso1_AI_2_5 NUMERIC,
    baget_uso1_AI_2_6 NUMERIC,
    baget_uso1_AI_2_7 NUMERIC,
    baget_uso1_AI_2_8 NUMERIC,
    baget_uso1_AI_3_1 NUMERIC,
    baget_uso1_AI_3_2 NUMERIC,
    baget_uso1_AI_3_3 NUMERIC,
    baget_uso1_AI_3_4 NUMERIC,
    baget_uso1_AI_3_5 NUMERIC,
    baget_uso1_AI_3_6 NUMERIC,
    baget_uso1_AI_3_7 NUMERIC,
    baget_uso1_AI_3_8 NUMERIC,
    baget_uso1_AI_4_1 NUMERIC,
    baget_uso1_AI_4_2 NUMERIC,
    baget_uso1_AI_4_3 NUMERIC,
    baget_uso1_AI_4_4 NUMERIC,
    baget_uso1_AI_4_5 NUMERIC,
    baget_uso1_AI_4_6 NUMERIC,
    baget_uso1_AI_4_7 NUMERIC,
    baget_uso1_AI_4_8 NUMERIC
);

INSERT INTO s0052382_tags (
    baget_plk_do_1_1,
    baget_plk_do_1_2,
    baget_plk_do_1_3,
    baget_plk_do_1_4,
    baget_plk_do_1_5,
    baget_plk_do_1_6,
    baget_plk_do_1_7,
    baget_plk_do_1_8,
    baget_plk_do_2_1,
    baget_plk_do_2_2,
    baget_plk_do_2_3,
    baget_plk_do_2_4,
    baget_plk_do_2_5,
    baget_plk_do_2_6,
    baget_plk_do_2_7,
    baget_plk_do_2_8
) VALUES (
        0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0
);

INSERT INTO s0052384_tags (
    baget_uso_DO_1_1,
    baget_uso_DO_1_2,
    baget_uso_DO_1_3,
    baget_uso_DO_1_4,
    baget_uso_DO_1_5,
    baget_uso_DO_1_6,
    baget_uso_DO_1_7,
    baget_uso_DO_1_8
) VALUES (
    1, 0, 1, 1, 0, 0, 1, 1
);

INSERT INTO controllers (
    controller, serial, description, address, tag_table
) VALUES (
    52381,
    's0052381',
    'R8 BAGET PLC MODULE digital inputs',
    34,
    's0052381_tags'
);

INSERT INTO controllers (
    controller, serial, description, address, tag_table
) VALUES (
    52382,
    's0052382',
    'R8 BAGET PLC MODULE digital outputs',
    34,
    's0052382_tags'
);

INSERT INTO controllers (
    controller, serial, description, address, tag_table
) VALUES (
    52383,
    's0052383',
    'R8 USO no.1 PLC MODULE digital inputs',
    35,
    's0052383_tags'
);

INSERT INTO controllers (
    controller, serial, description, address, tag_table
) VALUES (
    52384,
    's0052384',
    'R8 USO no.1 PLC MODULE digital outputs',
    35,
    's0052384_tags'
);

INSERT INTO controllers (
    controller, serial, description, address, tag_table
) VALUES (
    52385,
    's0052385',
    'R8 USO no.2 PLC MODULE analog inputs',
    36,
    's0052385_tags'
);

INSERT INTO controllers (
    controller, serial, description, address, tag_table
) VALUES (
    52386,
    's0052386',
    'R8 USO no.2 PLC MODULE analog outputs',
    36,
    's0052386_tags'
);
