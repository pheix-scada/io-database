PLATFORM := posix

ARCH=x64
PREFIX=
ECFLGS=$(ENV_CFLAGS)
CFLAGS=-static -Wall -Werror -g -fPIC -I./src/ `pkg-config --cflags libpq` -D__linux__ $(ECFLGS)
LDFLAGS=-lpthread -lm `pkg-config --libs libpq` -std=c99
OUTPUTPATH=debug
OUTPUTLIBPATH=$(OUTPUTPATH)/lib
OUTPUTHEADERS=$(OUTPUTLIBPATH)/include
SOURCES=$(wildcard src/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))
TESTSOURCES=$(wildcard t/*.c)
# LIBSOURCES=$(filter-out src/cli/cli.c,$(SOURCES))
LIBOBJECTS=$(patsubst %.c,%.o,$(SOURCES))

TESTTAGSSOURCES=$(filter-out t/iodb-test-mlcp-from-tab.c t/iodb-test-statuses.c t/iodb-test-events.c,$(TESTSOURCES))
TESTSTATUSSSOURCES=$(filter-out t/iodb-test-mlcp-from-tab.c t/iodb-test-tags.c t/iodb-test-events.c,$(TESTSOURCES))
TESTEVENTSSOURCES=$(filter-out t/iodb-test-mlcp-from-tab.c t/iodb-test-tags.c t/iodb-test-statuses.c,$(TESTSOURCES))
TESTBLDTABSOURCES=$(filter-out t/iodb-test-tags.c t/iodb-test-statuses.c t/iodb-test-events.c,$(TESTSOURCES))

LIBNAME=$(OUTPUTLIBPATH)/libiodb-$(ARCH).a
TARGET=debug/cli
TESTTAGS=debug/test-tags
TESTSTATUSES=debug/test-statuses
TESTEVENTS=debug/test-events
TESTBLDTAB=debug/test-bldtab

all: clean build lib
tests: clean test-tags test-statuses test-events test-build-tab
test-tags: build lib $(TESTTAGS)
test-statuses: build lib $(TESTSTATUSES)
test-events: build lib $(TESTEVENTS)
test-build-tab: build lib $(TESTBLDTAB)

#$(TARGET): build $(OBJECTS)
#	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)

$(TESTTAGS): build $(TESTTAGSSOURCES)
	$(CC) $(ECFLGS) -Wall -Werror -g -I./t -I$(OUTPUTHEADERS) $(TESTTAGSSOURCES) -L$(OUTPUTLIBPATH) -liodb-x64 $(LDFLAGS) -o $@

$(TESTSTATUSES): build $(TESTSTATUSSSOURCES)
	$(CC) $(ECFLGS) -Wall -Werror -g -I./t -I$(OUTPUTHEADERS) $(TESTSTATUSSSOURCES) -L$(OUTPUTLIBPATH) -liodb-x64 $(LDFLAGS) -o $@

$(TESTEVENTS): build $(TESTEVENTSSOURCES)
	$(CC) $(ECFLGS) -Wall -Werror -g -I./t -I$(OUTPUTHEADERS) $(TESTEVENTSSOURCES) -L$(OUTPUTLIBPATH) -liodb-x64 $(LDFLAGS) -o $@

$(TESTBLDTAB): build $(TESTBLDTABSOURCES)
	$(CC) $(ECFLGS) -Wall -Werror -g -I./t -I$(OUTPUTHEADERS) $(TESTBLDTABSOURCES) -L$(OUTPUTLIBPATH) -liodb-x64 $(LDFLAGS) -o $@

build:
	@mkdir -p $(OUTPUTPATH)

clean:
	rm -rf ./debug ./t/*.o ./src/*.o *.dSYM

lib: $(LIBOBJECTS)
	@mkdir -p $(OUTPUTHEADERS)
	@cp -f src/*.h $(OUTPUTHEADERS)
	@$(PREFIX)ar -r $(LIBNAME) $?
