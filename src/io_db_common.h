#ifndef __IO_DB_COMMON
#define __IO_DB_COMMON

#define STAT_OK           0
#define STAT_ER           -1
#define RECV_SIZE         65536
#define BUF_SIZE          RECV_SIZE + 1
#define NANOSLEEPVAL      50000000
#define READMODE          0
#define WRITEMODE         1
#define TAG_LEN           512
#define EVENT_PAYLOAD_LEN 2048

#ifndef DEBUG_PRINTS
    #define DEBUG_PRINTS 0
#endif

#endif //__IO_DB_COMMON
