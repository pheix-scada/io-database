#!/bin/sh

rm -f *.o
gcc -Wall -Werror -g -fPIC -I../debug/lib/include/ `pkg-config --cflags libpq` `pkg-config --libs libpq` -L../debug/lib/ -liodb-x64 -lpthread -c iodb.c