#ifndef __IO_DB_STRUCTS_H
#define __IO_DB_STRUCTS_H

#include <errno.h>
#include <semaphore.h>
#include <stddef.h>
#include <string.h>
#include <time.h>
#include <buffer.h>
#include <ctype.h>

#define ARRAY_SIZE(a) ((int)(sizeof(a) / sizeof((a)[0])))

typedef struct comm {
    int    com;
    double val;
} comm;

typedef struct tag {
    char   tagname[TAG_LEN];
    double val;
} tag;

typedef struct status_t {
    char statusname[32];
    int  main_status;
    char sub_status[32];
    int  reason_code;
    char reason_msg[TAG_LEN];
    char timestamp[32];
} status_t;

typedef struct event_t {
    char eventname[32];
    char timestamp[32];
    char payload[EVENT_PAYLOAD_LEN];

} event_t;

typedef struct mlcp_tags {
    sem_t        data_sync_sem;
    unsigned int controller_id;
    int          status;
    int          len;
    struct tag * tags;
    struct comm  command;
} mlcp_tags;

typedef struct mlcp_statuses_t {
    unsigned int    controller_id;
    int             len;
    struct status_t * statuses;
} mlcp_statuses_t;

typedef struct mlcp_events_t {
    unsigned int   controller_id;
    int            len;
    struct event_t * events;
} mlcp_events_t;

mlcp_tags * io_db_init_tags( unsigned int cntrl_id, char *tagnames[], int len );
void        io_db_free_tags( struct mlcp_tags * mlcp_t );
void        io_db_print_tags( struct mlcp_tags * mlcp_t );
double      io_db_access_tag( struct mlcp_tags * mlcp_t, char *tagname, unsigned int access_mode, double value );
int         io_db_set(struct mlcp_tags * mlcp_t, double * values, int len);

#endif //__IO_DB_STRUCTS_H
