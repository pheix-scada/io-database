#include <io_db.h>

int
io_db_set_events(struct mlcp_events_t * mlcp_events) {
    int i;
    int ret = 0;

    if (mlcp_events != NULL && mlcp_events->events != NULL && mlcp_events->len > 0) {
        for (i = 0; i < mlcp_events->len; i++) {
            Buffer * sql_buffer = buffer_alloc(BUF_SIZE);
            Buffer * values = buffer_alloc(BUF_SIZE);

            buffer_appendf(
                values,
                "%d,'%s','%s'",
                mlcp_events->controller_id,
                mlcp_events->events[i].eventname,
                mlcp_events->events[i].payload
            );

            buffer_appendf(
                sql_buffer,
                "INSERT INTO events (controller,event,payload) VALUES (%s);",
                buffer_to_s(values)
            );

            if (!io_db_pg_query(buffer_to_s(sql_buffer), NULL, 2)) {
                fprintf(
                    stderr,
                    "Store events to database failure! Query: %s\n",
                    buffer_to_s(sql_buffer)
                );
            } else {
                if (DEBUG_PRINTS) {
                    printf(
                        "Insert events: %s\n",
                        buffer_to_s(sql_buffer)
                    );
                }

                ret = 1;
            }

            buffer_free(sql_buffer);
            buffer_free(values);
        }
    }

    return ret;
}

mlcp_events_t *
io_db_get_events(
    unsigned int cntrl_id,
    char * eventsnames[],
    unsigned int len,
    unsigned int limit
) {
    int i;

    struct mlcp_events_t * mlcp_events = NULL;
    struct event_t * events = NULL;

    mlcp_events = calloc(1, sizeof(mlcp_events_t));

    if (len > 0 && eventsnames != NULL) {
        events = calloc(len, sizeof(event_t));

        if (mlcp_events != NULL && events != NULL) {
            mlcp_events->len = len;
            mlcp_events->controller_id = cntrl_id;

            for (i = 0; i < len; i++) {
                if (eventsnames[i] != NULL) {
                    strcpy(events[i].eventname, eventsnames[i]);
                }
            }

            mlcp_events->events = events;

            io_db_get_events_from_database(mlcp_events, limit);
        }
    }

    return mlcp_events;
}

int
io_db_get_events_from_database(struct mlcp_events_t * mlcp_events, unsigned int limit) {
    int i;
    int ret = 0;

    if (mlcp_events != NULL && mlcp_events->events != NULL && mlcp_events->len > 0) {
        Buffer * sql_buffer = buffer_alloc(BUF_SIZE);
        Buffer * clause = buffer_alloc(BUF_SIZE);

        for (i = 0; i < mlcp_events->len; i++) {
            buffer_appendf(
                clause,
                "'%s'%s",
                mlcp_events->events[i].eventname,
                i == (mlcp_events->len - 1) ? "" : ","
            );
        }

        buffer_appendf(
            sql_buffer,
            "SELECT created, event, payload FROM events WHERE event IN (%s) AND controller = %d ORDER BY created LIMIT %d;",
            buffer_to_s(clause),
            mlcp_events->controller_id,
            limit
        );

        if (!io_db_pg_query(buffer_to_s(sql_buffer), (void *)mlcp_events, 3)) {
            fprintf(
                stderr,
                "Fetch events from database failure! Query: %s\n",
                buffer_to_s(sql_buffer)
            );
        } else {
            if (DEBUG_PRINTS) {
                printf(
                    "Events: %s\n",
                    buffer_to_s(sql_buffer)
                );
            }

            ret = 1;
        }

        buffer_free(sql_buffer);
        buffer_free(clause);
    }
    return ret;
}

void
io_db_free_events(struct mlcp_events_t * mlcp_events) {
    if (mlcp_events != NULL) {
        if (mlcp_events->events != NULL) {
            free(mlcp_events->events);
        }

        free(mlcp_events);
    }
}

void
io_db_print_events(struct mlcp_events_t * mlcp_events) {
    int i;

    if (mlcp_events != NULL && mlcp_events->events != NULL && mlcp_events->len > 0) {
        for (i = 0; i < mlcp_events->len; i++) {
            printf(
                "\t%03d: %-16s => timestamp=%s, payload_len=%d, payload=%.32s...\n",
                i,
                mlcp_events->events[i].eventname,
                mlcp_events->events[i].timestamp,
                (int)strlen(mlcp_events->events[i].payload),
                mlcp_events->events[i].payload
            );
        }
    }
}
