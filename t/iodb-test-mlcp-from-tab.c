#include "iodb-test.h"

int
main(int argc, char * argv[]) {
    char devstr[TEST_STR_SZ];

    if (IODB_VERSION_DEV) {
        sprintf(devstr, ".dev-%d", IODB_VERSION_DEV);
    }

    printf(
        "***INF (0x%08x): libiodb version: %d.%d.%d%s\n",
        (int) pthread_self(),
        IODB_VERSION_MAJOR,
        IODB_VERSION_MINOR,
        IODB_VERSION_RELEASE,
        (IODB_VERSION_DEV > 0) ? devstr : ""
    );

    io_db_pg_version();

    if (!io_db_pg_srv_version()) {
        fprintf(
            stderr,
            "***ERR (0x%08x): Postgres is unavailable\n",
            (int) pthread_self()
        );
    };

    struct mlcp_tags * mlcp_t = io_db_get_all_tags(CONTROLLER);

    if (mlcp_t != NULL) {
        printf("***INF (0x%08x): built from tab test for controller %d is passed\n", (int)pthread_self(), CONTROLLER);
    }
    else {
        fprintf(
            stderr,
            "***ERR (0x%08x): built from tab failure\n",
            (int)pthread_self()
        );
    }

    return 0;
}
