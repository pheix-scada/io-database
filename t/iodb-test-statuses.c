#include "iodb-test.h"

struct mlcp_statuses_t * populate_statuses(unsigned int len);

int
main(int argc, char * argv[]) {
    int i = 0;
    unsigned int len = TEST_STATUSES + TEST_GEN_STATUSES;

    struct mlcp_statuses_t * mlcp_statuses_write = NULL;
    struct mlcp_statuses_t * mlcp_statuses_read = NULL;
    char devstr[TEST_STR_SZ];

    if (IODB_VERSION_DEV) {
        sprintf(devstr, ".dev-%d", IODB_VERSION_DEV);
    }

    printf(
        "***INF (0x%08x): libiodb version: %d.%d.%d%s\n",
        (int) pthread_self(),
        IODB_VERSION_MAJOR,
        IODB_VERSION_MINOR,
        IODB_VERSION_RELEASE,
        (IODB_VERSION_DEV > 0) ? devstr : ""
    );

    io_db_pg_version();

    if (!io_db_pg_srv_version()) {
        fprintf(
            stderr,
            "***ERR (0x%08x): Postgres is unavailable\n",
            (int) pthread_self()
        );
    };

    mlcp_statuses_write = populate_statuses(TEST_GEN_STATUSES);

    if (!io_db_set_statuses(mlcp_statuses_write)) {
        fprintf(
            stderr,
            "***ERR (0x%08x): Can not store statuses\n",
            (int) pthread_self()
        );
    } else {
        if (DEBUG_PRINTS) {
            printf(
                "***INF (0x%08x): stored %d/%d statuses\n",
                (int) pthread_self(),
                mlcp_statuses_write -> len,
                len
            );
        }

        io_db_free_statuses(mlcp_statuses_write);
    }

    char ** test_status_names = calloc(len, TEST_STAT_SZ * sizeof(char));

    for (i = 0; i < len; i++) {
        if (i < 2) {
            test_status_names[i] = statuses[i];
        } else {
            test_status_names[i] = names[i - 2];
        }
    }

    mlcp_statuses_read = io_db_get_statuses(CONTROLLER, test_status_names, len);

    if (mlcp_statuses_read != NULL && mlcp_statuses_read -> statuses != NULL && mlcp_statuses_read -> len > 0) {
        io_db_free_statuses(mlcp_statuses_read);
    } else {
        fprintf(
            stderr,
            "***ERR (0x%08x): no statuses found\n",
            (int) pthread_self()
        );
    }

    return 0;
}

struct mlcp_statuses_t *
populate_statuses(unsigned int len) {
    int i;

    struct mlcp_statuses_t * mlcp_statuses = NULL;
    struct status_t * statuses = NULL;

    mlcp_statuses = calloc(1, sizeof(mlcp_statuses_t));

    if (len > 0 && mlcp_statuses != NULL) {
        statuses = calloc(len, sizeof(status_t));

        if (statuses != NULL) {
            mlcp_statuses -> len = len;
            mlcp_statuses -> controller_id = CONTROLLER;

            for (i = 0; i < len; i++) {
                if (statuses != NULL) {
                    sprintf(statuses[i].statusname, "%s", names[i]);
                    sprintf(statuses[i].sub_status, "sub_%s_%d", names[i], i);
                    sprintf(statuses[i].reason_msg, "reason_%s", statuses[i].sub_status);
                    statuses[i].main_status = 100 + i;
                    statuses[i].reason_code = 1000 + i;
                }
            }

            mlcp_statuses -> statuses = statuses;
        }
    }

    return mlcp_statuses;
}
