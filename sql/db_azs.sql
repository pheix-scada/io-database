DROP TABLE if exists s0000574_tags;
CREATE TABLE s0000574_tags (
    id            SERIAL PRIMARY KEY,
    timestamp     char(20) DEFAULT extract(epoch from now()),
    level         NUMERIC,
    volume        NUMERIC,
    water         NUMERIC,
    temperature   NUMERIC,
    density       NUMERIC,
    level_tank1         NUMERIC,
    volume_tank1        NUMERIC,
    water_tank1         NUMERIC,
    temperature_tank1   NUMERIC,
    density_tank1       NUMERIC,
    level_tank2         NUMERIC,
    volume_tank2        NUMERIC,
    water_tank2         NUMERIC,
    temperature_tank2   NUMERIC,
    density_tank2       NUMERIC,
    level_tank3         NUMERIC,
    volume_tank3        NUMERIC,
    water_tank3         NUMERIC,
    temperature_tank3   NUMERIC,
    density_tank3       NUMERIC,
    level_tank4         NUMERIC,
    volume_tank4        NUMERIC,
    water_tank4         NUMERIC,
    temperature_tank4   NUMERIC,
    density_tank4       NUMERIC,
    level_tank5         NUMERIC,
    volume_tank5        NUMERIC,
    water_tank5         NUMERIC,
    temperature_tank5   NUMERIC,
    density_tank5       NUMERIC,
    ready_led           NUMERIC,
    error_led           NUMERIC
);
INSERT INTO controllers (
    controller, serial, description, address, tag_table
) VALUES (
   574,
   's0000574',
   'BT74 MODULE',
   33,
   's0000574_tags'
);
