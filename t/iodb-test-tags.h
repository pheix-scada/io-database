#ifndef __IODB_TEST_TAGS_H
#define __IODB_TEST_TAGS_H

#define TEST_STAT_SZ 32
#define TEST_STR_SZ  64
#define TEST_SENSORS 61
#define TEST_STATUSES 2
#define TEST_GEN_STATUSES 58
#define TEST_EVENTS 4
#define TEST_EVENTS_LIMIT 200

#define TEST_MAGIC_TAG_NAME  "ub_bkhc"
#define TEST_MAGIC_TAG_DATA  1982.22

char * events[TEST_EVENTS] = {
    "event_alert",
    "event_info",
    "event_warning",
    "event_failure"
};

char * statuses[TEST_STAT_SZ] = {
    "plc_reserved",
    "plc_reboot"
};

char * names[TEST_STR_SZ] = {
    "Qb_AD2",
    "Qb_BKHC",
    "Qb_DHC",
    "Qb_DKC",
    "Qb_KP",
    "Qb_TP20",
    "Qc_AD",
    "Qc_AD2",
    "Qc_BKHC",
    "Qc_DHC",
    "Qc_DKC",
    "Qc_TP20",
    "Sa_AD",
    "Sa_AD2",
    "Sa_BKHC",
    "Sb_AD",
    "Sb_AD2",
    "Sb_BKHC",
    "Sb_DHC",
    "Sb_DKC",
    "Sb_KP",
    "Sb_TP20",
    "Sc_AD",
    "Sc_AD2",
    "Sc_BKHC",
    "Sc_DHC",
    "Sc_DKC",
    "Sc_KP",
    "Sc_TP20",
    "Ua_AD",
    "Ua_AD2",
    "Ua_BKHC",
    "Ua_DKC",
    "Ua_KP",
    "Ua_TP20",
    "Ub_AD",
    "Ub_AD2",
    "Ub_BKHC",
    "Ub_DKC",
    "Ub_KP",
    "Ub_TP20",
    "Uc_AD",
    "Uc_AD2",
    "Uc_BKHC",
    "Uc_DKC",
    "Uc_KP",
    "cosFi_AD",
    "cosFi_BKHC",
    "cosFi_DHC",
    "cosFi_DKC",
    "cosFi_KP",
    "cosFi_TP20",
    "tag_close_AD",
    "tag_close_AD2",
    "tag_close_BKHC",
    "tag_close_DHC",
    "tag_close_DKC",
    "tag_close_KP",
    "tag_close_TP20",
    "tag_close_bus",
    "WIRE_on"
};

#endif // __IODB_TEST_TAGS_H
