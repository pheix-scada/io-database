#include <io_db_pg.h>

void io_db_pg_version(void)
{
#ifndef __OCPB__
    int lib_ver = PQlibVersion();
    printf("Version of libpq: %d\n", lib_ver);
#else
    if (DEBUG_PRINTS) {
        printf("No PostgreSQL\n");
    }
#endif
    return (void)NULL;
}

int io_db_pg_srv_version(void) {
#ifndef __OCPB__
    PGconn *conn = PQconnectdb(CONNECTION_STRING);
    if (PQstatus(conn) == CONNECTION_BAD) {
        fprintf(stderr, "Connection to database from io_db_pg_srv_versio() failed: %s\n",
            PQerrorMessage(conn));
        PQfinish(conn);
        return 0;
    }
    int ver = PQserverVersion(conn);
    if (DEBUG_PRINTS) {
        printf("Server version: %d\n", ver);
    }
    PQfinish(conn);
#else
    if (DEBUG_PRINTS) {
        printf("No PostgreSQL\n");
    }
#endif
    return 1;
}

int io_db_pg_query(const char *query, void * mlcp_container, int container_type)
{
#ifndef __OCPB__
    int i;
    PGconn *conn = PQconnectdb(CONNECTION_STRING);
    if (PQstatus(conn) == CONNECTION_BAD) {
        fprintf(
            stderr,
            "Connection to database from io_db_pg_query() failed: %s\n",
            PQerrorMessage(conn)
        );

        PQfinish(conn);

        return 0;
    }

    PGresult *res = PQexec(conn, query);

    if (
        PQresultStatus(res) != PGRES_TUPLES_OK &&
        PQresultStatus(res) != PGRES_COMMAND_OK
    ){
        if (DEBUG_PRINTS) {
            printf("No data retrieved or command failed\n");
        }
        PQclear(res);
        PQfinish(conn);
        return 0;
    }
    else {
        if (PQresultStatus(res) == PGRES_TUPLES_OK && PQntuples(res)) {
            int colums = PQnfields(res);

            /* work with tags */
            if (container_type == 1) {
                double * values = calloc(colums, sizeof(double));
                for(i = 0; i < colums; i++) {
                    values[i] = strtod(PQgetvalue(res, 0, i), NULL);
                    if (DEBUG_PRINTS) {
                        printf(
                            "%s: %.03f\n",
                            PQfname(res, i),
                            values[i]
                        );
                    }
                }

                if (!io_db_import_tags((struct mlcp_tags *)mlcp_container, values, colums)) {
                    fprintf(
                        stderr,
                        "Could not set values fetched from database to MLCP tags\n"
                    );

                    return 0;
                }
                else {
                    if (DEBUG_PRINTS) {
                        printf("%s\n", query);

                        io_db_print_tags(mlcp_container);
                    }
                }

                free(values);
            }

            /* work with statuses */
            if (container_type == 2) {
                int rows = PQntuples(res);

                struct mlcp_statuses_t * mlcp_statuses = (struct mlcp_statuses_t *)mlcp_container;

                if (rows > 0 && mlcp_statuses != NULL && mlcp_statuses->statuses != NULL && mlcp_statuses->len > 0) {
                    for(i = 0; i < rows; i++) {
                        strcpy(mlcp_statuses->statuses[i].timestamp, PQgetvalue(res, i, 0));
                        strcpy(mlcp_statuses->statuses[i].sub_status, PQgetvalue(res, i, 3));
                        strcpy(mlcp_statuses->statuses[i].reason_msg, PQgetvalue(res, i, 5));
                        mlcp_statuses->statuses[i].main_status = strtod(PQgetvalue(res, i, 2), NULL);
                        mlcp_statuses->statuses[i].reason_code = strtod(PQgetvalue(res, i, 4), NULL);
                    }

                    if (DEBUG_PRINTS) {
                        printf("Fetched %d rows (tuples)\n", rows);

                        io_db_print_statuses(mlcp_statuses);
                    }
                }
                else {
                    fprintf(stderr, "No rows (tuples) fetched\n");

                    return 0;
                }
            }

            /* work with events */
            if (container_type == 3) {
                int rows = PQntuples(res);

                struct mlcp_events_t * mlcp_events = (struct mlcp_events_t *)mlcp_container;

                if (rows > 0 && mlcp_events != NULL) {
                    struct event_t * events = calloc(rows, sizeof(event_t));

                    if (events != NULL) {
                        if (mlcp_events->events != NULL && mlcp_events->len > 0) {
                            free(mlcp_events->events);
                        }

                        mlcp_events->len = rows;

                        for(i = 0; i < rows; i++) {
                            strcpy(events[i].timestamp, PQgetvalue(res, i, 0));
                            strcpy(events[i].eventname, PQgetvalue(res, i, 1));
                            strcpy(events[i].payload, PQgetvalue(res, i, 2));
                        }

                        mlcp_events->events = events;

                        if (DEBUG_PRINTS) {
                            printf("Fetched %d rows (tuples)\n", rows);

                            io_db_print_events(mlcp_events);
                        }
                    }
                }
                else {
                    fprintf(stderr, "No rows (tuples) fetched\n");

                    return 0;
                }
            }
        }
    }
    PQclear(res);
    PQfinish(conn);
#else
    if (DEBUG_PRINTS) {
        printf("No PostgreSQL\n");
    }
#endif
    return 1;
}

struct mlcp_tags * io_db_pg_mlcp_from_tab(const char *query, unsigned int controller_id)
{
#ifndef __OCPB__
    int i = 0;
    struct mlcp_tags * mlcp_t = NULL;

    PGconn *conn = PQconnectdb(CONNECTION_STRING);

    if (PQstatus(conn) == CONNECTION_BAD) {
        fprintf(
            stderr,
            "Connection to database from io_db_pg_query() failed: %s\n",
            PQerrorMessage(conn)
        );

        PQfinish(conn);

        return NULL;
    }

    PGresult *res = PQexec(conn, query);

    if (
        PQresultStatus(res) != PGRES_TUPLES_OK &&
        PQresultStatus(res) != PGRES_COMMAND_OK
    ){
        if (DEBUG_PRINTS) {
            printf("No data retrieved or command failed\n");
        }

        PQclear(res);
        PQfinish(conn);

        return NULL;
    }
    else {
        if (PQresultStatus(res) == PGRES_TUPLES_OK && PQntuples(res)) {
            int rows = PQntuples(res);

            unsigned int tag_len = rows - 2;

            if (tag_len > 0) {
                int j = 0;

                char ** tag_names = calloc(tag_len, sizeof(char*));

                for (i = 0; i < rows; i++) {
                    if (strcmp(PQgetvalue(res, i, 0), "id") == 0 || strcmp(PQgetvalue(res, i, 0), "timestamp") == 0) {
                        continue;
                    }

                    tag_names[j++] = strdup(PQgetvalue(res, i, 0));
                }

                if (tag_names != NULL) {
                    mlcp_t = io_db_init_tags(controller_id, tag_names, tag_len);

                    return mlcp_t;
                }
            }
            else {
                fprintf(
                    stderr,
                    "Invalid tags length for controller %d\n",
                    controller_id
                );

                return NULL;
            }
        }
    }

    PQclear(res);
    PQfinish(conn);
#else
    if (DEBUG_PRINTS) {
        printf("No PostgreSQL\n");
    }
#endif
    return NULL;
}
