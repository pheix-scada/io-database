#ifndef __IO_DB_H
#define __IO_DB_H

#include <io_db_structs.h>

struct mlcp_tags * io_db_get_all_tags(int controller_id);
int io_db_set_tags(struct mlcp_tags * mlcp_t);
int io_db_get_tags(struct mlcp_tags * mlcp_t);
int io_db_set_command(unsigned int cntrl_id, struct comm * command);
int io_db_set_command_status(unsigned int cntrl_id, int command, unsigned int status);
int io_db_get_command_status(unsigned int cntrl_id, int command);

/* statuses */
void io_db_free_statuses(struct mlcp_statuses_t * mlcp_statuses);
void io_db_print_statuses(struct mlcp_statuses_t * mlcp_statuses);
int io_db_get_statuses_from_database(struct mlcp_statuses_t * mlcp_statuses);
int io_db_set_statuses(struct mlcp_statuses_t * mlcp_statuses);
mlcp_statuses_t * io_db_get_statuses(unsigned int cntrl_id, char * statusesnames[], int len);

/* events */
mlcp_events_t * io_db_get_events(unsigned int cntrl_id, char * eventsnames[], unsigned int len, unsigned int limit);
int io_db_get_events_from_database(struct mlcp_events_t * mlcp_events, unsigned int limit);
void io_db_free_events(struct mlcp_events_t * mlcp_events);
void io_db_print_events(struct mlcp_events_t * mlcp_events);
int io_db_set_events(struct mlcp_events_t * mlcp_events);

extern int io_db_pg_query(const char *query, void * mlcp_container, int container_type);
extern struct mlcp_tags * io_db_pg_mlcp_from_tab(const char *query, unsigned int controller_id);

#endif // __IO_DB_PG_H
