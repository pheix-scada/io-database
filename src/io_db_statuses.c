#include <io_db.h>

int
io_db_set_statuses(struct mlcp_statuses_t * mlcp_statuses) {
    int i;
    int ret = 0;

    if (mlcp_statuses != NULL && mlcp_statuses->statuses != NULL && mlcp_statuses->len > 0) {
        for (i = 0; i < mlcp_statuses->len; i++) {
            Buffer * sql_buffer = buffer_alloc(BUF_SIZE);
            Buffer * colums = buffer_alloc(BUF_SIZE);
            Buffer * values = buffer_alloc(BUF_SIZE);

            buffer_appendf(colums, "controller,status,main_status,sub_status,reason_code,reason_msg");

            buffer_appendf(
                values,
                "%d,'%s',%d,'%s',%d,'%s'",
                mlcp_statuses->controller_id,
                mlcp_statuses->statuses[i].statusname,
                mlcp_statuses->statuses[i].main_status,
                mlcp_statuses->statuses[i].sub_status,
                mlcp_statuses->statuses[i].reason_code,
                mlcp_statuses->statuses[i].reason_msg
            );

            buffer_appendf(
                sql_buffer,
                "INSERT INTO statuses (%s) VALUES (%s);",
                buffer_to_s(colums),
                buffer_to_s(values)
            );

            if (!io_db_pg_query(buffer_to_s(sql_buffer), NULL, 2)) {
                fprintf(
                    stderr,
                    "Store statuses to database failure! Query: %s\n",
                    buffer_to_s(sql_buffer)
                );
            } else {
                if (DEBUG_PRINTS) {
                    printf(
                        "Insert statuses: %s\n",
                        buffer_to_s(sql_buffer)
                    );
                }

                ret = 1;
            }

            buffer_free(sql_buffer);
            buffer_free(colums);
            buffer_free(values);
        }
    }

    return ret;
}

mlcp_statuses_t *
    io_db_get_statuses(unsigned int cntrl_id, char * statusesnames[], int len) {
    int i;

    struct mlcp_statuses_t * mlcp_statuses = NULL;
    struct status_t * statuses = NULL;

    mlcp_statuses = calloc(1, sizeof(mlcp_statuses_t));

    if (len > 0 && statusesnames != NULL) {
        statuses = calloc(len, sizeof(status_t));

        if (mlcp_statuses != NULL && statuses != NULL) {
            mlcp_statuses->len = len;
            mlcp_statuses->controller_id = cntrl_id;

            for (i = 0; i < len; i++) {
                if (statusesnames[i] != NULL) {
                    strcpy(statuses[i].statusname, statusesnames[i]);
                }
            }

            mlcp_statuses->statuses = statuses;

            io_db_get_statuses_from_database(mlcp_statuses);
        }
    }

    return mlcp_statuses;
}

int
io_db_get_statuses_from_database(struct mlcp_statuses_t * mlcp_statuses) {
    int i;
    int ret = 0;

    if (mlcp_statuses != NULL && mlcp_statuses->statuses != NULL && mlcp_statuses->len > 0) {
        Buffer * sql_buffer = buffer_alloc(BUF_SIZE);
        Buffer * clause = buffer_alloc(BUF_SIZE);

        for (i = 0; i < mlcp_statuses->len; i++) {
            buffer_appendf(
                clause,
                "'%s'%s",
                mlcp_statuses->statuses[i].statusname,
                i == (mlcp_statuses->len - 1) ? "" : ","
            );
        }

        buffer_appendf(
            sql_buffer,
            "SELECT created, status, main_status, sub_status, reason_code, reason_msg FROM statuses WHERE status IN (%s) AND controller = %d ORDER BY created;",
            buffer_to_s(clause),
            mlcp_statuses->controller_id
        );

        if (!io_db_pg_query(buffer_to_s(sql_buffer), (void * ) mlcp_statuses, 2)) {
            fprintf(
                stderr,
                "Fetch statuses from database failure! Query: %s\n",
                buffer_to_s(sql_buffer)
            );
        } else {
            if (DEBUG_PRINTS) {
                printf(
                    "Statuses: %s\n",
                    buffer_to_s(sql_buffer)
                );
            }

            ret = 1;
        }

        buffer_free(sql_buffer);
        buffer_free(clause);
    }
    return ret;
}

void
io_db_free_statuses(struct mlcp_statuses_t * mlcp_statuses) {
    if (mlcp_statuses != NULL) {
        if (mlcp_statuses->statuses != NULL) {
            free(mlcp_statuses->statuses);
        }

        free(mlcp_statuses);
    }
}

void
io_db_print_statuses(struct mlcp_statuses_t * mlcp_statuses) {
    int i;

    if (mlcp_statuses != NULL && mlcp_statuses->statuses != NULL && mlcp_statuses->len > 0) {
        for (i = 0; i < mlcp_statuses->len; i++) {
            printf(
                "\t%03d: %-16s => timestamp=%s, main_status=%d, sub_status=%s, reason_code=%d, reason_msg=%s\n",
                i,
                mlcp_statuses->statuses[i].statusname,
                mlcp_statuses->statuses[i].timestamp,
                mlcp_statuses->statuses[i].main_status,
                mlcp_statuses->statuses[i].sub_status,
                mlcp_statuses->statuses[i].reason_code,
                mlcp_statuses->statuses[i].reason_msg
            );
        }
    }
}
