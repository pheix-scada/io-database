#include <io_db.h>

int io_db_set_tags(struct mlcp_tags * mlcp_t)
{
    int i, j;
    int ret = 0;

    if (mlcp_t != NULL && mlcp_t->tags != NULL && mlcp_t->len > 0) {
        Buffer *sql_buffer = buffer_alloc(BUF_SIZE);
        Buffer *colums     = buffer_alloc(BUF_SIZE);
        Buffer *values     = buffer_alloc(BUF_SIZE);

        struct mlcp_tags * mlcp_all = NULL;

        mlcp_all = io_db_get_all_tags(mlcp_t->controller_id);

        if (mlcp_all != NULL && mlcp_all->tags) {
            for(i = 0; i < mlcp_t->len; i++) {
                for(j = 0; j < mlcp_all->len; j++) {
                    if (strcmp(mlcp_all->tags[j].tagname, mlcp_t->tags[i].tagname) == 0) {
                        mlcp_all->tags[j].val = mlcp_t->tags[i].val;
                    }
                }
            }
        }
        else {
            mlcp_all = io_db_init_tags(0, NULL, 0);

            if (mlcp_all != NULL) {
                mlcp_all->len = mlcp_t->len;
                mlcp_all->controller_id = mlcp_t->controller_id;
                mlcp_all->tags = calloc(mlcp_t->len, sizeof(tag));

                memcpy(mlcp_all->tags, mlcp_t->tags, mlcp_t->len * sizeof(tag));
            }
        }

        if (mlcp_all != NULL) {
            for(i = 0; i < mlcp_all->len; i++) {
                buffer_appendf(
                    colums,
                    "%s%s",
                    mlcp_all->tags[i].tagname,
                    i == (mlcp_all->len - 1) ? "" : ","
                );
                buffer_appendf(
                    values,
                    "%.05f%s",
                    (double)mlcp_all->tags[i].val,
                    i == (mlcp_all->len - 1) ? "" : ","
                );
            }

            buffer_appendf(
                sql_buffer,
                "INSERT INTO s%07d_tags (%s) VALUES (%s);",
                mlcp_all->controller_id,
                buffer_to_s(colums),
                buffer_to_s(values)
            );

            if (!io_db_pg_query(buffer_to_s(sql_buffer), NULL, 1)) {
                fprintf(
                    stderr,
                    "Store tags to database failure! Query: %s\n",
                    buffer_to_s(sql_buffer)
                );
            }
            else {
                ret = 1;
            }

            buffer_free(sql_buffer);
            buffer_free(colums);
            buffer_free(values);

            io_db_free_tags(mlcp_all);
        }
    }
    return ret;
}

int io_db_get_tags(struct mlcp_tags * mlcp_t)
{
    int i;
    int ret = 0;
    if (mlcp_t != NULL && mlcp_t->tags != NULL && mlcp_t->len > 0) {
        Buffer *sql_buffer = buffer_alloc(BUF_SIZE);
        Buffer *colums     = buffer_alloc(BUF_SIZE);

        for(i=0; i<mlcp_t->len; i++) {
            buffer_appendf(
                colums,
                "%s%s",
                mlcp_t->tags[i].tagname,
                i == (mlcp_t->len - 1) ? "" : ","
            );
        }

        buffer_appendf(
            sql_buffer,
            "SELECT %s FROM s%07d_tags ORDER BY timestamp DESC LIMIT 1;",
            buffer_to_s(colums),
            mlcp_t->controller_id
        );

        if (!io_db_pg_query(buffer_to_s(sql_buffer), (void *)mlcp_t, 1)) {
            fprintf(
                stderr,
                "Fetch tags from database failure! Query: %s\n",
                buffer_to_s(sql_buffer)
            );
        }
        else {
            ret = 1;
        }

        buffer_free(sql_buffer);
        buffer_free(colums);
    }
    return ret;
}

struct mlcp_tags * io_db_get_all_tags(int controller_id)
{
    struct mlcp_tags * mclp_t = NULL;
    Buffer *sql_buffer = buffer_alloc(BUF_SIZE);

    buffer_appendf(
        sql_buffer,
        "SELECT column_name FROM information_schema.columns WHERE table_schema = 'public' AND table_name = 's%07d_tags'",
        controller_id
    );

    mclp_t = io_db_pg_mlcp_from_tab(buffer_to_s(sql_buffer), controller_id);

    if (mclp_t != NULL) {
        if(io_db_get_tags(mclp_t)) {
            return mclp_t;
        }
    }
    else {
        fprintf(
            stderr,
            "Build MLCP from table failure! Query: %s\n",
            buffer_to_s(sql_buffer)
        );
    }

    buffer_free(sql_buffer);

    return NULL;
}

int io_db_set_command(unsigned int cntrl_id, struct comm * command)
{
    return 0;
}

int io_db_set_command_status(
    unsigned int cntrl_id,
    int command,
    unsigned int status
)
{
    return 0;
}

int io_db_get_command_status(unsigned int cntrl_id, int command)
{
    return 0;
}
