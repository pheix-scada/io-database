#include <stdio.h>
#include <pthread.h>
#include <io_db.h>
#include <io_db_ver.h>

/* #define CONTROLLER   574 */
#define TEST_STR_SZ  64
#define TEST_SENSORS 5

static int controller_id = 0;

extern void io_db_pg_version(void);
extern int io_db_pg_srv_version(void);
extern struct mlcp_tags * io_db_init_tags(unsigned int cntrl_id, char *tagnames[], int len);
extern int io_db_set_tags(struct mlcp_tags * mlcp_t);
extern void io_db_free_tags(struct mlcp_tags * mlcp_t);
extern int io_db_import_tags(struct mlcp_tags * mlcp_t, double * values, int len);

int set_controller_id(int cntrl_id);
int get_controller_id(void);
double * iodb_select(char **keys, int len, int cntrl_id);
int iodb_insert(char **keys, double * values, int len, int cntrl_id);
int iodb_rnd_insert(int cntrl_id);
int iodb_debug_in(char **keys, double * values, int len);
double * iodb_debug_out(char **keys, int len);

int
set_controller_id(int cntrl_id)
{
    if (cntrl_id > 0) {
        controller_id = cntrl_id;
        return controller_id;
    }
    else {
        return 0;
    }
}

int
get_controller_id(void) {
    return controller_id;
}

double *
iodb_select(char **keys, int len, int cntrl_id)
{
    int i;
    double * values = calloc(len, sizeof(double));
    if (set_controller_id(cntrl_id)) {
        struct mlcp_tags * mlcp_t =
            io_db_init_tags( controller_id, keys, len );
        if ( mlcp_t != NULL ) {
            if (io_db_get_tags(mlcp_t)) {
                for (i = 0; i < mlcp_t->len; i++) {
                    if (mlcp_t->tags != NULL) {
                        values[i] = mlcp_t->tags[i].val;
                    }
                }
            }
            io_db_free_tags(mlcp_t);
        }
    }
    return values;
}

int
iodb_insert(char **keys, double * values, int len, int cntrl_id)
{
    int ret = 1;
    if (set_controller_id(cntrl_id)) {
        struct mlcp_tags * mlcp_t =
            io_db_init_tags( controller_id, keys, len );
        if ( mlcp_t != NULL ) {
            io_db_import_tags(mlcp_t, values, len);
            if (!io_db_set_tags(mlcp_t)) {
                ret = 0;
            }
            io_db_free_tags(mlcp_t);
        }
    }
    return ret;
}

int
iodb_rnd_insert(int cntrl_id)
{
    char * names[TEST_STR_SZ] = {
        "level",
        "volume",
        "water",
        "temperature",
        "density",
    };
    srand(time(NULL));
    double values[TEST_SENSORS] = {
        (double)rand()/RAND_MAX,
        (double)rand()/RAND_MAX,
        (double)rand()/RAND_MAX,
        (double)rand()/RAND_MAX,
        (double)rand()/RAND_MAX
    };
    /*
    io_db_pg_version();
    if (!io_db_pg_srv_version()) {
        return 0;
    };
    */
    return iodb_insert(names, (double *)values, TEST_SENSORS, cntrl_id);
}

int
iodb_debug_in(char **keys, double * values, int len)
{
    int i;
    for (i = 0; i < len; i++) {
        if (keys[i]) {
            printf(
                "IN: keys[%d] = %s, values[%d] = %.03f\n",
                i,
                keys[i],
                i,
                values[i]
            );
        }
    }
    return 1;
}

double *
iodb_debug_out(char **keys, int len)
{
    double * values = NULL;
    int i;
    if (len > 0) {
        values = calloc(len, sizeof(double));
        srand(time(NULL));
        for (i = 0; i < len; i++) {
            if (keys[i]) {
                values[i] = (double)rand()/RAND_MAX*2.0-1.0;
                printf(
                    "OUT(%d): keys[%d] = %s, values[%d] = %.03f\n",
                    len,
                    i,
                    keys[i],
                    i,
                    values[i]
                );
            }
        }
    }
    return values;
}
