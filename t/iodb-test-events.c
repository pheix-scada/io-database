#include "iodb-test.h"

char * random_json_request(unsigned int seed);
struct mlcp_events_t * store_new_events(struct mlcp_events_t * mlcp_events, unsigned int len);
char ** get_events_names(struct mlcp_events_t * mlcp_events);

int
main(int argc, char * argv[]) {
    unsigned int len = TEST_EVENTS;

    struct mlcp_events_t * mlcp_events = NULL;
    char devstr[TEST_STR_SZ];

    if (IODB_VERSION_DEV) {
        sprintf(devstr, ".dev-%d", IODB_VERSION_DEV);
    }

    printf(
        "***INF (0x%08x): libiodb version: %d.%d.%d%s\n",
        (int) pthread_self(),
        IODB_VERSION_MAJOR,
        IODB_VERSION_MINOR,
        IODB_VERSION_RELEASE,
        (IODB_VERSION_DEV > 0) ? devstr : ""
    );

    io_db_pg_version();

    if (!io_db_pg_srv_version()) {
        fprintf(
            stderr,
            "***ERR (0x%08x): Postgres is unavailable\n",
            (int) pthread_self()
        );
    };

    char ** test_event_names = events;

    mlcp_events = io_db_get_events(CONTROLLER, test_event_names, len, TEST_EVENTS_LIMIT);

    store_new_events(mlcp_events, 10);

    char ** updated_event_names = get_events_names(mlcp_events);

    mlcp_events = io_db_get_events(CONTROLLER, updated_event_names, mlcp_events->len, TEST_EVENTS_LIMIT);

    if (mlcp_events != NULL && mlcp_events->events != NULL && mlcp_events->len > 0) {
        io_db_free_events(mlcp_events);
    } else {
        fprintf(
            stderr,
            "***ERR (0x%08x): no events found\n",
            (int)pthread_self()
        );
    }

    free(updated_event_names);

    return 0;
}

struct mlcp_events_t *
store_new_events(struct mlcp_events_t * mlcp_events, unsigned int len) {
    int i;

    struct mlcp_events_t * mlcp_new_events = calloc(1, sizeof(mlcp_events_t));

    struct event_t * new_events = NULL;
    struct event_t * events = NULL;

    if (len > 0 && mlcp_events != NULL && mlcp_new_events != NULL) {
        unsigned int newlen = mlcp_events->len + len;

        events     = calloc(newlen, sizeof(event_t));
        new_events = calloc(len, sizeof(event_t));

        if (events != NULL && new_events != NULL) {
            for (i = 0; i < newlen; i++) {
                if (i < mlcp_events->len) {
                    events[i] = mlcp_events->events[i];
                }
                else {
                    strcpy(events[i].eventname, names[i]);
                    strcpy(events[i].payload, random_json_request((i ^ len * CONTROLLER) + (int)pthread_self()));

                    strcpy(new_events[i - mlcp_events->len].eventname, events[i].eventname);
                    strcpy(new_events[i - mlcp_events->len].payload, events[i].payload);
                }
            }

            mlcp_events->len = newlen;
            mlcp_events->controller_id = CONTROLLER;
            mlcp_events->events = events;

            mlcp_new_events->len = len;
            mlcp_new_events->controller_id = CONTROLLER;
            mlcp_new_events->events = new_events;

            if (!io_db_set_events(mlcp_new_events)) {
                fprintf(
                    stderr,
                    "***ERR (0x%08x): Can not store events\n",
                    (int)pthread_self()
                );
            } else {
                if (DEBUG_PRINTS) {
                    printf(
                        "***INF (0x%08x): stored %d events\n",
                        (int)pthread_self(),
                        mlcp_events->len
                    );
                }
            }

            return mlcp_events;
        }
    }

    return NULL;
}

char *
random_json_request(unsigned int seed) {
	FILE *fptr;
    size_t bytes = 0;
    char * payload = calloc(EVENT_PAYLOAD_LEN, sizeof(char));
    char buff[TAG_LEN];
    char file[TAG_LEN];

    srand(seed);

    sprintf(file, "./t/requests/%02d.json", (rand() % 16));

	fptr = fopen(file, "r");

    if (fptr == NULL)
	{
        fprintf(stderr,"***ERR: cannot open file %s\n", file);

        return NULL;
	}

	while (fgets(buff, TAG_LEN, fptr) != NULL && bytes < EVENT_PAYLOAD_LEN) {
        buff[strcspn(buff, "\n")] = 0;

        bytes = bytes + strlen(buff);

        strcat(payload, buff);
    }

    if (DEBUG_PRINTS) {
        printf("***INF (0x%08x): file %s requested\n", (int)pthread_self(), file);
    }

	fclose(fptr);

    return payload;
}

char **
get_events_names(struct mlcp_events_t * mlcp_events) {
    unsigned int i = 0;

    printf("%d", TEST_STAT_SZ);

    if (mlcp_events != NULL && mlcp_events->events != NULL && mlcp_events->len > 0) {
        char ** updated_names = calloc(mlcp_events->len, (TEST_STAT_SZ) * sizeof(char));

        for (i = 0; i < mlcp_events->len; i++) {
            printf("%s", mlcp_events->events[i].eventname);
            updated_names[i] = mlcp_events->events[i].eventname;
        }

        return updated_names;
    }

    return NULL;
}
