DROP TABLE if exists controllers;
DROP TABLE if exists statuses;
DROP TABLE if exists debug;
DROP TABLE if exists events;

CREATE TABLE controllers (
    id             serial PRIMARY KEY,
    created        timestamp without time zone DEFAULT now(),
    modified       timestamp without time zone DEFAULT now(),
    controller     integer NOT NULL,
    serial         varchar(32) NOT NULL,
    name           varchar(32),
    description    varchar(64) NOT NULL,
    role           varchar(32),
    address        integer NOT NULL,
    type           varchar(32),
    interface_list text,
    tag_table      varchar(32) NOT NULL
);

CREATE TABLE statuses (
    id          serial PRIMARY KEY,
    created     timestamp without time zone DEFAULT now(),
    modified    timestamp without time zone DEFAULT now(),
    controller  integer NOT NULL,
    status      varchar(32) NOT NULL,
    main_status integer NOT NULL,
    sub_status  varchar(32),
    reason_code integer NOT NULL,
    reason_msg  varchar(256)
);

CREATE TABLE debug (
    id         serial PRIMARY KEY,
    created    timestamp without time zone DEFAULT now(),
    modified   timestamp without time zone DEFAULT now(),
    controller integer NOT NULL,
    reason     integer NOT NULL,
    message    varchar(32),
    dump       text NOT NULL
);

CREATE TABLE events (
    id         serial PRIMARY KEY,
    created    timestamp without time zone DEFAULT now(),
    modified   timestamp without time zone DEFAULT now(),
    controller integer NOT NULL,
    event      varchar(32) NOT NULL,
    payload    json NOT NULL
);
