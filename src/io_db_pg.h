#ifndef __IO_DB_PG_H
#define __IO_DB_PG_H

#include <stdlib.h>
#include <buffer.h>
#ifndef __OCPB__
    #include <libpq-fe.h>
    #define CONNECTION_STRING "host=127.0.0.1 port=5432 user=postgres password=ospasswd dbname=idc"
#endif
#include <io_db_structs.h>

void io_db_pg_version(void);
int io_db_pg_srv_version(void);
int io_db_pg_query(const char *query, void * mlcp_container, int container_type);
struct mlcp_tags * io_db_pg_mlcp_from_tab(const char *query, unsigned int controller_id);

extern int io_db_import_tags(struct mlcp_tags * mlcp_t, double * values, int len);
extern void io_db_print_tags( struct mlcp_tags * mlcp_t );
extern void io_db_print_statuses(struct mlcp_statuses_t * mlcp_statuses);
extern void io_db_print_events(struct mlcp_events_t * mlcp_events);

#endif // __IO_DB_PG_H
